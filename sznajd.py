from random import random
from matplotlib import markers
import numpy as np
import matplotlib.pyplot as plt
import imageio
import os

def FlipSpin(s,i,j):
    if s[i,j] < 0:
        s[i,j] = 1
    else:
        s[i,j] = -1

def getNeighbors(s,i,j):
    m = np.shape(s)[0]
    n = np.shape(s)[1]
    neighbor_coords = [[(i-1)%m, j%n], [i%m, (j-1)%n], [i%m, (j+1)%n], [(i+1)%m, j%n]]
    return neighbor_coords

def main():
    n = 50
    rounds_per_rho = 1000
    interval = 1000
    N = 100000000
    R = 11
    rho0 = np.linspace(0.5, 0.6, R)
    results = np.zeros((R, rounds_per_rho), int)
    steps_to_consensus = np.zeros((R, rounds_per_rho), int)

    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 12

    make_gif = False
    filenames = []

    for ri in range(len(rho0)):
        filenames = []
        for nn in range(rounds_per_rho):
            s = np.array([])
            nums = -np.ones(n*n, int)
            nums[:int(rho0[ri]*n*n)] = 1
            #print(int(rho0[ri]*m*n))
            np.random.shuffle(nums)
            s = np.reshape(nums, (n,n))

            ii = 0
            voteSum = 0
            fig_index = 0
            while abs(voteSum) != np.size(s) and ii < N:
                i = int(np.random.random()*n)
                j = int(np.random.random()*n)

                neighbor_coords = getNeighbors(s,i,j)
                random_neighbor = neighbor_coords[int(np.random.rand()*4)]
                for ij in getNeighbors(s,random_neighbor[0], random_neighbor[1]):
                    neighbor_coords.append(ij)

                opinion = s[i,j]
                if opinion == s[random_neighbor[0],random_neighbor[1]]:
                    for ij in neighbor_coords:
                        s[ij[0],ij[1]] = opinion
                
                voteSum = np.sum(s)

                if make_gif and nn == 0 and ii % interval == 0:
                    plt.figure(figsize=(4,4))
                    plt.imshow(s)
                    plt.axis("off")
                    path = "./figs_sznajd/"+str(fig_index)+".png"
                    plt.savefig(path)
                    filenames.append(path)
                    #plt.show()
                    plt.close()
                    print(path)
                    fig_index += 1

                ii += 1
            steps_to_consensus[ri, nn] = ii
            results[ri, nn] = voteSum > 0
            print(nn)

        # Build gif
        if make_gif and nn == 0:
            with imageio.get_writer(("ani_sznajd"+str(ri)+".gif"), mode='I') as writer:
                for filename in filenames:
                    image = imageio.imread(filename)
                    writer.append_data(image)
            
            # Remove files'
            
            for filename in os.listdir("./figs_sznajd"):
                os.remove("./figs_sznajd/"+filename)
                
        print("Initial rho: {0:.2f} Results: {1:.2f}".format(rho0[ri], np.mean(results[ri])))

    np.save("sznajd_results.npy", results)
    np.save("sznajd_steps.npy", steps_to_consensus)

    fig = plt.figure()
    plt.plot(rho0, np.mean(results, axis=1),marker="x", linestyle="")
    #plt.plot([0, 1], [0, 1], linestyle='dotted', color="r")
    plt.xlabel("Initial density of +1 sites")
    plt.ylabel("Probability of +1 consensus")
    plt.savefig("fig_sznajd_probability")
    plt.figure()
    plt.plot(rho0, np.mean(steps_to_consensus, axis=1), marker="x", linestyle="")
    plt.ylabel("Steps to consensus")
    plt.xlabel("Initial density of +1 sites")
    plt.savefig("fig_sznajd_steps_to_consensus.png")
    plt.show()

if __name__ == "__main__":
    main()