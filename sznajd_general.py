from cProfile import label
from random import random
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
import imageio
import os

def FlipSpin(s,i,j):
    if s[i,j] < 0:
        s[i,j] = 1
    else:
        s[i,j] = -1

def getNeighbors(s,i,j):
    n = np.shape(s)[0]
    neighbor_coords = [[(i-1)%n, j%n], [i%n, (j-1)%n], [i%n, (j+1)%n], [(i+1)%n, j%n]]
    return neighbor_coords

def main():
    n = 50
    rounds_per_rho = 50
    interval = n*n
    limit = 100000000
    R = 5
    opinions = [2,4,8,16,32,64]
    #rho0 = np.linspace(0, 1, R)
    results = np.zeros((len(opinions), rounds_per_rho), int)
    steps_to_consensus = np.zeros((len(opinions), rounds_per_rho), int)

    make_gif = False
    filenames = []

    evolution = np.ones((len(opinions), rounds_per_rho, limit//interval))

    for ri in range(len(opinions)):
        for nn in range(rounds_per_rho):
            s = np.array([])
            nums = np.zeros(n*n, int)
            colors = opinions[ri]
            nums = np.random.randint(0,colors,n*n)
            #print(int(rho0[ri]*m*n))
            np.random.shuffle(nums)
            s = np.reshape(nums, (n,n))

            ii = 0
            voteSum = 0
            fig_index = 0
            agreement = False
            while not agreement and ii < limit:
                i = int(np.random.random()*n)
                j = int(np.random.random()*n)

                neighbor_coords = getNeighbors(s,i,j)
                random_neighbor = neighbor_coords[int(np.random.rand()*4)]
                for ij in getNeighbors(s,random_neighbor[0], random_neighbor[1]):
                    neighbor_coords.append(ij)

                opinion = s[i,j]
                if opinion == s[random_neighbor[0],random_neighbor[1]]:
                    for ij in neighbor_coords:
                        s[ij[0],ij[1]] = opinion
                
                agreement = s.max() == s.min()

                if make_gif and ri == 0 and nn == 0 and ii % interval == 0:
                    plt.figure(figsize=(4,4))
                    ax = plt.imshow(s,vmin = 0, vmax = colors,cmap="hsv")
                    plt.tight_layout()
                    plt.axis("off")
                    path = "./figs_sznajd_general/"+str(fig_index)+".png"
                    plt.savefig(path)
                    filenames.append(path)
                    #plt.show()
                    plt.close()
                    print(path)
                    fig_index += 1
                
                if ii%interval==0:
                    evolution[ri,nn,fig_index] = np.max(np.histogram(s, bins=(np.arange(colors)-0.5))[0])/n**2
                    fig_index += 1


                ii += 1


            steps_to_consensus[ri, nn] = ii
            results[ri, nn] = voteSum > 0
            
        print("Initial number of opinions: {0:.2f} Steps: {1:.2f}".format(opinions[ri], np.mean(steps_to_consensus[ri])))

    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 12

    # Build gif
    if make_gif:
        with imageio.get_writer('ani_sznajd_general.gif', mode='I') as writer:
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)
        
        # Remove files
        for filename in os.listdir("./figs_sznajd_general"):
            os.remove("./figs_sznajd_general/"+filename)

    fig = plt.figure()
    print(np.shape(evolution))
    a = np.max(steps_to_consensus)//interval
    print(a)
    evolution = evolution[:,:,:a]
    evolution = np.mean(evolution, axis=1)
    print(np.shape(evolution))
    for i in range(len(opinions)):
        plt.plot(evolution[i], marker='.', linestyle="", label="Initial  opinions: {0}".format(opinions[i]))
        #plt.plot([0, 1], [0, 1], linestyle='dotted', color="r")
    plt.xlabel(r"Time (steps / $N$)")
    plt.ylabel(r"$\langle S \rangle$")
    plt.legend()
    #plt.title(r"Evolution of the largest cluster")
    plt.savefig("fig_sznajd_general_evolution.png")
    plt.show()

if __name__ == "__main__":
    main()