import numpy as np
import matplotlib.pyplot as plt
import os

def FlipSpin(s,i,j):
    if s[i,j] < 0:
        s[i,j] = 1
    else:
        s[i,j] = -1

def getRandomNeighbor1D(s,i):
    n = np.shape(s)[0]
    neighbors = [s[(i-1)%n], s[(i+1)%n]]
    return neighbors[int(2*np.random.rand())]


def getRandomNeighbor2D(s,i,j):
    m = np.shape(s)[0]
    n = np.shape(s)[1]
    neighbors = [s[(i-1)%m, j%n], s[i%m, (j-1)%n], s[i%m, (j+1)%n], s[(i+1)%m, j%n]]
    return neighbors[int(4*np.random.rand())]

def getRandomNeighbor3D(s,i,j,k):
    n = np.shape(s)[0]
    neighbors = [s[(i-1)%n, j%n, k%n], s[i%n, (j-1)%n, k%n], s[i%n, (j+1)%n, k%n], s[(i+1)%n, j%n, k%n], s[i%n, j%n, (k-1)%n], s[i%n, j%n, (k+1)%n]]
    return neighbors[int(6*np.random.rand())]

def main():
    sys_dim = 2
    n = 10
    rounds_per_rho = 5000
    interval = 100
    N = 100000
    R = 11
    rho0 = np.linspace(0, 1, R)
    results = np.zeros((R, rounds_per_rho), int)
    steps_to_consensus = np.zeros((R, rounds_per_rho), int)

    make_gif = False
    filenames = []
    fig_index = 0

    for ri in range(len(rho0)):
        filenames = []
        for nn in range(rounds_per_rho):
            s = np.array([])
            if sys_dim == 1:
                s = -np.ones(n)
                s[:int(rho0[ri]*n)] = 1
                #print(int(rho0[ri]*m*n))
                np.random.shuffle(s)
            if sys_dim == 2:
                nums = -np.ones(n*n)
                nums[:int(rho0[ri]*n*n)] = 1
                #print(int(rho0[ri]*m*n))
                np.random.shuffle(nums)
                s = np.reshape(nums, (n,n))
            if sys_dim == 3:
                nums = -np.ones(n**3)
                nums[:int(rho0[ri]*n**3)] = 1
                #print(int(rho0[ri]*m*n))
                np.random.shuffle(nums)
                s = np.reshape(nums, (n,n,n))
            ii = 0
            voteSum = 0
            fig_index = 0
            while abs(voteSum) != np.size(s) and ii < N:
                i = int(np.random.random()*n)
                j = 0
                k = 0

                if sys_dim == 2:
                    j = int(np.random.random()*n)
                    s[i,j] = getRandomNeighbor2D(s,i,j)

                if sys_dim == 3:
                    j = int(np.random.random()*n)
                    k = int(np.random.random()*n)
                    s[i,j,k] = getRandomNeighbor3D(s,i,j,k)

                if sys_dim == 1:
                    s[i] = getRandomNeighbor1D(s,i)
                
                voteSum = np.sum(s)
                ii += 1

                if make_gif and nn == 0 and ii % interval == 0:
                    plt.figure(figsize=(4,4))
                    plt.imshow(s)
                    plt.axis("off")
                    path = "./figs_voter/"+str(fig_index)+".png"
                    plt.savefig(path)
                    filenames.append(path)
                    #plt.show()
                    plt.close()
                    print(path)
                    fig_index += 1
            steps_to_consensus[ri, nn] = ii
            results[ri, nn] = voteSum > 0
            
        print("Initial rho: {0:.2f} Results: {1:.2f}".format(rho0[ri], np.mean(results[ri])))

        if make_gif and nn == 0:
            with imageio.get_writer(("ani_voter"+str(ri)+".gif"), mode='I') as writer:
                for filename in filenames:
                    image = imageio.imread(filename)
                    writer.append_data(image)
            
            # Remove files
            for filename in os.listdir("./figs_voter"):
                os.remove("./figs_voter/"+filename)


    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 14
    fig = plt.figure(figsize=(5,5))
    plt.plot(rho0, np.mean(results, axis=1), marker='x', linestyle="")
    plt.plot([0, 1], [0, 1], linestyle='dotted', color="r")
    plt.xlabel(r"$\rho_0$")
    plt.ylabel("Probability of +1 consensus")
    plt.savefig("fig_voter_prob_consensus.png")
    plt.figure(figsize=(5,5))
    plt.plot(rho0, np.mean(steps_to_consensus, axis=1),marker='x', linestyle="")
    linear_model=np.polyfit(rho0,np.mean(steps_to_consensus, axis=1),2)
    linear_model_fn=np.poly1d(linear_model)
    x = np.linspace(0,1,50)
    #plt.plot(x,linear_model_fn(x),color="r", linestyle='dotted')
    plt.xlabel(r"$\rho_0$")
    plt.ylabel(r"MC steps to consensus")
    plt.savefig("fig_steps_to_consensus.png")

    plt.show()

if __name__ == "__main__":
    main()