import numpy as np
import matplotlib.pyplot as plt
import imageio
import os

def main():
    i = 1
    fig = plt.figure(figsize=(10,5))
    for m in [0.3, 0.6]:
        for epsilon in [10, 50, 100]:
            fig = main1(epsilon, m, fig, i)
            print("Epsilon: {0}, m: {1}".format(epsilon, m))
            i += 1
    plt.tight_layout()
    plt.savefig("fig_relative.png")
    plt.show()

def main1(epsilon, m, fig, fig_i):
    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 12

    N = 2500
    rounds = 500000
    if epsilon == 10:
        rounds *=3
    interval = N
    x = rounds // interval

    make_gif = False

    s = np.uint8(255*np.random.rand(N))

    filenames = []
    fig_index = 0

    evolution = np.zeros((256,x))
    ii = 0
    for r in range(rounds):
        i = 0
        j = 0
        while i == j:
            i = int(np.random.rand()*N)
            j = int(np.random.rand()*N)
        
        if s[j] > s[i]:
            a = j
            j = i
            i = a

        dif = s[i]-s[j]
        #print(dif)

        if dif < epsilon:
            s[j] += int((m/2)*dif)
            s[i] -= int((m/2)*dif)
            #print(int((m/2)*dif))

        if r % interval == 0:
            for jj in range(len(s)):
                evolution[s[jj],ii] += 1
            ii += 1

        if make_gif and r % interval == 0:
            plt.hist(s, bins=np.linspace(-0.5,255.5,257), color='r')
            path = "./figs_relative/"+str(fig_index)+".png"
            plt.ylim([0, 120])
            plt.xlabel("Opinion")
            plt.ylabel("Opinion density")
            plt.title("Opinion distribution $\epsilon = {0:.2}, m = {1:.2}, t = {2}$".format(epsilon/N, m, ii))
            plt.gca().set_xticks(np.array([0, 0.2, 0.4, 0.6, 0.8, 1.0])*256)
            plt.gca().set_xticklabels([0, 0.2, 0.4, 0.6, 0.8, 1.0])
            plt.yticks([])
            plt.savefig(path)
            filenames.append(path)
            plt.close()
            print(path)
            fig_index += 1
    
    # Build gif
    if make_gif:
        with imageio.get_writer("ani_relative_epsilon{0}_m{1:.0f}.gif".format(epsilon, m*10), mode='I') as writer:
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)
        
        # Remove files
        for filename in os.listdir("./figs_relative"):
            os.remove("./figs_relative/"+filename)

    ax = fig.add_subplot(2,3,fig_i)
    ax.contourf(evolution, levels=np.linspace(1,np.max(evolution),10))
    if fig_i in [4,5,6]:
        plt.xlabel(r"MC time (steps / N)")
    if fig_i in [1,4]:
        plt.ylabel(r"Opinion")
    plt.gca().set_yticks([0, 128, 256])
    plt.gca().set_yticklabels([0, 0.5, 1.0])
    plt.title("$\epsilon = {0:.2}, m = {1:.2}$".format(epsilon/N, m))
    print("Ready")
    #plt.show()
    return fig


if __name__ == "__main__":
    main()