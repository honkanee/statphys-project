import numpy as np
import matplotlib.pyplot as plt

def FlipSpin(s,i,j):
    if s[i,j] < 0:
        s[i,j] = 1
    else:
        s[i,j] = -1

def getRandomNeighbor1D(s,i):
    n = np.shape(s)[0]
    neighbors = [s[(i-1)%n], s[(i+1)%n]]
    return neighbors[int(2*np.random.rand())]

def getRandomNeighbor2D(s,i,j):
    m = np.shape(s)[0]
    n = np.shape(s)[1]
    neighbors = [s[(i-1)%m, j%n], s[i%m, (j-1)%n], s[i%m, (j+1)%n], s[(i+1)%m, j%n]]
    return neighbors[int(4*np.random.rand())]

def getRandomNeighbor3D(s,i,j,k):
    n = np.shape(s)[0]
    neighbors = [s[(i-1)%n, j%n, k%n], s[i%n, (j-1)%n, k%n], s[i%n, (j+1)%n, k%n], s[(i+1)%n, j%n, k%n], s[i%n, j%n, (k-1)%n], s[i%n, j%n, (k+1)%n]]
    return neighbors[int(6*np.random.rand())]

def run_voter(sys_dim, n, rho0, rounds):
    n=int(n)
    interval = 100
    limit = 1000000000
    results = np.zeros((rounds), int)
    steps_to_consensus = np.zeros((rounds), int)
    for nn in range(rounds):
        s = np.array([])
        if sys_dim == 1:
            s = -np.ones(n)
            s[:int(rho0*n)] = 1
            #print(int(rho0[ri]*m*n))
            np.random.shuffle(s)
        if sys_dim == 2:
            nums = -np.ones(n*n)
            nums[:int(rho0*n*n)] = 1
            #print(int(rho0[ri]*m*n))
            np.random.shuffle(nums)
            s = np.reshape(nums, (n,n))
        if sys_dim == 3:
            nums = -np.ones(n**3)
            nums[:int(rho0*n**3)] = 1
            #print(int(rho0[ri]*m*n))
            np.random.shuffle(nums)
            s = np.reshape(nums, (n,n,n))
        ii = 0
        voteSum = 0
        while abs(voteSum) != np.size(s) and ii < limit:
            i = int(np.random.random()*n)
            j = 0
            k = 0

            if sys_dim == 2:
                j = int(np.random.random()*n)
                s[i,j] = getRandomNeighbor2D(s,i,j)

            if sys_dim == 3:
                j = int(np.random.random()*n)
                k = int(np.random.random()*n)
                s[i,j,k] = getRandomNeighbor3D(s,i,j,k)

            if sys_dim == 1:
                s[i] = getRandomNeighbor1D(s,i)
            
            voteSum = np.sum(s)
            ii += 1
        steps_to_consensus[nn] = ii
        results[nn] = voteSum > 0
        print("Size: {0}, Steps: {1}".format(n**sys_dim, steps_to_consensus[nn]))

    return np.mean(steps_to_consensus), np.mean(results)


def main():

    rounds_per_conf = 25
    a = 11
    N = np.round(np.linspace(10, 500, a))
    ns = np.zeros((3,a))

    steps = 0
    steps_to_consensus = np.zeros((3,a))
    for i in range(a):
        ns[0,i] = N[i]
        steps, result = run_voter(1,ns[0,i], 0.5, rounds_per_conf)
        steps_to_consensus[0, i] = steps
        ns[1,i] = np.round(np.sqrt(N[i]))
        steps, result = run_voter(2,ns[1,i], 0.5, rounds_per_conf)
        steps_to_consensus[1, i] = steps
        ns[2,i] = np.round(N[i]**(1/3))
        if i in [0,1,2,3,5,7,9]:
            steps, result = run_voter(3,ns[2,i], 0.5, rounds_per_conf)
        else:
            steps = steps_to_consensus[2,i-1]
        steps_to_consensus[2,i] = steps

    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 14

    # in other dimensions Ns = [64, 729, 1000]
    
    
    fig = plt.figure()
    plt.plot(ns[0,:], steps_to_consensus[0,:], marker='x', linestyle='')
    plt.plot(ns[1,:]**2, steps_to_consensus[1,:], marker= 'x', linestyle='')
    plt.plot(ns[2,:]**3, steps_to_consensus[2,:], marker='x', linestyle='')
    plt.yscale('log')
    plt.legend([r"$d = 1$", r"$d = 2", r"$d = 3$"])
    #plt.plot(, linestyle='dotted', color="r")
    plt.xlabel(r"$N$")
    plt.ylabel("Steps to consensus")
    plt.legend([r"$d = 1$", r"$d = 2$", r"$d = 3$"])
    #plt.title(r"Number of iterations as a function of $N$")
    plt.savefig("fig_voter_model_iterations_dimensions.png")
    plt.show()

if __name__ == "__main__":
    main()